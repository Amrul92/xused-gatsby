import React from 'react'
import Helmet from 'react-helmet'
import { prefixLink } from 'gatsby-helpers'
import { TypographyStyle, GoogleFont } from 'react-typography'
import typography from './utils/typography'

const BUILD_TIME = new Date().getTime()

const html = (props) => {
  const head = Helmet.rewind()

  let css
  if (process.env.NODE_ENV === 'production') {
    css = <style dangerouslySetInnerHTML={{ __html: require('!raw!./public/styles.css') }} />
  }

  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" /> {head
          .title
          .toComponent()}
        {head
          .meta
          .toComponent()}
        <TypographyStyle typography={typography} />
        <GoogleFont typography={typography} /> {css}
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        {/*<link rel="stylesheet" href="https://unpkg.com/react-select/dist/react-select.css">*/}

      </head>
      <body>
        <div id="react-mount" dangerouslySetInnerHTML={{ __html: props.body }} />
        <script src={prefixLink(`/bundle.js?t=${BUILD_TIME}`)} />
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvxuMBYKrn034iLAFIS-8-71SlgQw0YqY&v=3.exp&libraries=places" />
        <script dangerouslySetInnerHTML={{ __html: `(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-83944239-1', 'auto');
      ga('send', 'pageview');
   //       window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
   // d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
   // _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
   // $.src="//v2.zopim.com/?4E1WTnV8e8WozVF4kzKuBCw49Vf1zbXv";z.t=+new Date;$.
   // type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
          ` }} />
      </body>
    </html>
  )
}

html.propTypes = {
  body: React.PropTypes.string,
}

export default html
