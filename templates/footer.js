import React from 'react'
import { Link } from 'react-router'
import I18n from '../utils/i18n'

const Footer = (props) => {
  const i18n = new I18n(props.language)
  return (
    <footer>
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <ul className="list-inline">
              <li>
                <Link to={i18n.link('about')}> {i18n.strings.footer.about} </Link>
              </li>
              <li className="footer-menu-divider">&sdot;</li>
              <li>
                <Link to={i18n.link('faq')}>{i18n.strings.footer.faq}</Link>
              </li>
              <li className="footer-menu-divider">&sdot;</li>
              <li>
                <Link to={i18n.link('contact')}>{i18n.strings.footer.contact}</Link>
              </li>
            </ul>
            <ul className="list-inline">
              <li>
                <a href="https://www.facebook.com/xused/" target="_blank" rel="noopener noreferrer"><i className="fa fa-facebook fa-fw" /></a>
              </li>
              <li>
                <a href="https://twitter.com/Xused2016" target="_blank" rel="noopener noreferrer"><i className="fa fa-twitter fa-fw" /></a>
              </li>
              <li>
                <a href="https://www.instagram.com/xusedmy/" target="_blank" rel="noopener noreferrer"><i className="fa fa-instagram fa-fw" /></a>
              </li>
            </ul>
            <p className="copyright text-muted small">&copy; XUsed 2016. {i18n.strings.footer.reserved}</p>
          </div>
        </div>
      </div>
    </footer>
  )
}

Footer.propTypes = {
  language: React.PropTypes.string,
}

export default Footer
