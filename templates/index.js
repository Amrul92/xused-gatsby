import React from 'react'
import Helmet from 'react-helmet'

import { Link } from 'react-router'
import { prefixLink } from 'gatsby-helpers'
import { Grid, Row, Col } from 'react-bootstrap'
import { config } from 'config'
import { Icon } from 'react-fa'

import hasRefer from '../utils/refer.js'
import I18n from '../utils/i18n.js'
import Navbar from './navbar'
import Footer from './footer'
// import img from '../images/img'
import '../css/main.css'

const FeatureItem = (props) => {
  const offsetClass = props.index % 2 ? 'col-lg-5 col-sm-6' : 'col-lg-5 col-lg-offset-1 col-sm-push-6 col-sm-6'
  const offsetImage = props.index % 2 ? 'col-lg-5 col-lg-offset-2 col-sm-6' : 'col-lg-5 col-sm-pull-6 col-sm-6'
  return (
    <div className="container">
      <div className="row">
        <div className={offsetClass}>
          <hr className="section-heading-spacer" />
          <div className="clearfix" />
          <h2 className="section-heading">{props.heading}</h2>
          <p className="lead">{props.description}</p>
        </div>
        <div className={offsetImage}>
          <div
            style={{ height: '400px' }}
            className={`img-features-${props.index + 1} img-responsive img-${props.image}`}
          />
          {/* <img
            className={`img-features-${props.index + 1} img-responsive`}
            src={img[props.image]} alt=""
          /> */}
        </div>
      </div>
    </div>
  )
}

FeatureItem.propTypes = {
  index: React.PropTypes.number,
  heading: React.PropTypes.string,
  description: React.PropTypes.string,
  image: React.PropTypes.string,
}

const Features = props => {
  const features = props.i18n.strings.index.features
  .map((item, index) => <FeatureItem key={index} index={index} {...item} />)
  return (<div>{features}</div>)
}

const HowTo = props => (
  <div className="container">
    <div className="col-lg-12">
      <h2 className="page-header">{props.i18n.strings.index.HowTo[0]}</h2>
      {props.i18n.strings.index.HowTo[1].map(role => {
        return (
        <div key={role.title} className="col-lg-6">
          <div className="panel panel-success">
            <div className="panel-heading">
              <h3 className="panel-title">{role.title}</h3>
            </div>
            <div className="panel-body">
              <ol>
                {role.steps.map(step => (<li key={step}>{step}</li>))}
              </ol>
            </div>
          </div>
        </div>
      )})}
    </div>
  </div>
)

const ItemCard = props => (
  <div className="col-sm-6 col-md-4">
    <div className="index_thumbnail thumbnail">
      <div className={`item_image_index img-${props.image}`} alt={props.type} />
      <div className="caption">
        <h3>{props.type}</h3>
        <p>{props.prompt} {props.currency} {props.price}/KG</p>
      </div>
    </div>
  </div>
)

ItemCard.propTypes = {
  image: React.PropTypes.string,
  type: React.PropTypes.string,
  prompt: React.PropTypes.string,
  currency: React.PropTypes.string,
  price: React.PropTypes.number,
  view: React.PropTypes.string,
  onHold: React.PropTypes.bool,
}

const ItemTypes = props => (
  <div className="container">
    {/* Type of Waste Items Section */}
    <div className="row">
      <div className="col-lg-12">
        <h2 className="page-header">{props.i18n.strings.items.title}</h2>
      </div>
      {Object.keys(props.i18n.strings.items.types).map((type) => {
        const item = props.i18n.strings.items.types[type]
        item.prompt = props.i18n.strings.items.prompt
        item.currency = props.i18n.strings.items.currency
        item.view = item.onHold ? props.i18n.strings.items.soon : props.i18n.strings.items.view
        return (<ItemCard key={item.type} {...item} />)
      })}
    </div>
  </div>
)

const FAQItem = props => (
  <div className="col-md-6">
    <div className={'faq-item'+props.classAdd}>
      <Icon className="faq-item-icon" name="question-circle" />
      <h4 className="faq-item-heading">{props.title}</h4>
      <div className="faq-item-text">{props.content}</div>
    </div>
  </div>
)

FAQItem.propTypes = {
  title: React.PropTypes.string,
  content: React.PropTypes.string,
}

const FAQ = props => (
  <div className="container">
    {/* FAQ */}
    <div className="row">
      <div className="col-lg-12">
        <h2 className="page-header">{props.i18n.strings.faq.title}</h2>
          {props.i18n.strings.faq.items.slice(0, 4).map((item, index) => (
            <FAQItem {...item} key={index + item.title} classAdd={index < 2 ? '-1' : ''} />
          ))}
      </div>
    </div>
    {/* END FAQ */}
  </div>
)

export default class props extends React.Component {

  componentDidMount() {
    // currently being tracked in _template.html
    // might need it if we want to look at events on the index page
    // const reactGA = require('../utils/googleanalytics')
    // reactGA.trackPage()
    const refer = hasRefer()

    if (refer) {
      localStorage.setItem('refer', refer.ref)
    }

    // TODO: check that user isn't logged in to hide signup button
  }

  render() {
    const i18n = new I18n(this.props.language)
    return (
      <div>
        <Navbar className="navbar_custom" routes={this.props.location} language={i18n.language} />
        <Helmet
          title={config.siteTitle}
          meta={[
            { name: 'description', content: i18n.strings.meta.description },
            { name: 'keywords', content: 'Recycle, Earn Money, Save Environment, Waste Items' },
            { property: 'og:title', content: i18n.strings.meta.title},
            { property: 'og:url', content: `https://app.xused.com/${i18n.language}/` },
            { property: 'og:description', content: i18n.strings.meta.description },
            { property: 'og:image', content: 'https://app.xused.com/images/logos/XUsed_Logo_213x209.png' },
            { property: 'fb:app_id', content: '1695458107363799' },
          ]}
        />
        <div className="intro-header">
          <Grid>
            <Row>
              <Col lg={12}>
                <div className="intro-message">
                  <h1><strong>XUsed</strong></h1>
                  <h3> { i18n.strings.index.hero.discover } </h3>
                  <ul className="list-inline intro-social-buttons">
                    <li>
                      <Link to={{ pathname: i18n.link('register'), query: hasRefer() }} className="btn btn-signup-hero btn-success btn-lg">
                        <span className="hero-index">
                          { i18n.strings.index.hero.sign_up }
                        </span>
                      </Link>
                    </li>
                  </ul>
                </div>
              </Col>
            </Row>
          </Grid>
        </div>


        {/* Page Content */}

        <div className="content-section-a">
          <Features i18n={i18n} />
          <HowTo i18n={i18n} />
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                {/* Video */}
                <div className="video-container">
                  <iframe src="https://www.youtube.com/embed/ZaV6-rtyKLE" frameBorder="0" allowFullScreen />
                </div>
              </div>
            </div>
          </div>
          <ItemTypes i18n={i18n} />
          <FAQ i18n={i18n} />
        </div>
        <Footer language={this.props.language} />
      </div>
    )
  }
}
