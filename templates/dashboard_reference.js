import React from 'react'

const DashboardReference = props => (
    <article className="col-md-6">
      <div className="panel panel-pricing-guide panel-success">
        <div className="panel-heading">
          <h3 className="panel-title">{props.i18n.strings.dashboard_reference.items_pricing}</h3>
        </div>
        <div className="panel-body">
          <table className="table">
            <thead><th className="pricing_type">{props.i18n.strings.dashboard_reference.pricing_type}</th><th className="pricing_type">{props.i18n.strings.dashboard_reference.pricing_quantity}</th></thead>
            <tbody>
              {Object.keys(props.i18n.strings.items.types).map(
                (itemKey) => {
                  const items = props.i18n.strings.items
                  const fullItem = items.types[itemKey]
                  const price = `${items.currency} ${parseFloat(fullItem.price).toFixed(2)}`
                  const comingSoon = fullItem.onHold ? props.i18n.strings.items.soon : price
                  return (
                    <tr>
                      <td className="items_type_td">{fullItem.type}</td>
                      <td className="items_pricing_td">{comingSoon}</td>
                    </tr>)
                }
              )}
            </tbody>
          </table>
        </div>
      </div>
    </article>
)

export default DashboardReference
