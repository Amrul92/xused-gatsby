import React from 'react'
import {
  Link,
} from 'react-router'
import Helmet from 'react-helmet'
import SweetAlert from 'react-bootstrap-sweetalert'
import { Button, Table } from 'react-bootstrap'
import { ShareButtons, generateShareIcon } from 'react-share'
// import CopyToClipboard from 'react-copy-to-clipboard'

import {
  config,
} from 'config'
import I18n from '../utils/i18n.js'
import '../css/main.css'
import Navbar from './navbar'
import Footer from './footer'
import DashboardReference from './dashboard_reference'
import UpdateItem from './update_item'
import UpdateAllItems from './update_all_items'

import firebase from '../utils/firebase'
import WarnCountry from './warncountry'
import '../node_modules/react-bootstrap-sweetalert/lib/css/animations.css'

const FacebookIcon = generateShareIcon('facebook')
const TwitterIcon = generateShareIcon('twitter')

const WasteItem = props => (
  <article className="col-sm-6 col-md-3">
    <div className="panel panel-item-card panel-success">
      <div className="panel-heading">
        <h3 className="panel-title">{props.i18n.strings.items.types[props.type].type}</h3>
      </div>
      <div className="panel-seller-dashboard panel-body">
        <div className={`img-responsive center-block img-${props.image}`} style={{ height: '200px' }} />
        <div className="list_items">
          {props.quantity} KG
        </div>
        {props.quantity < 5 ? props.i18n.strings.seller_dashboard.guideline : <br />}
        <div className="progress">
          <div
            className={`progress-bar ${props.quantity < 5 ? 'progress-bar-danger' : ''} progress-bar-striped activeprentice women's hospital chicago`}
            role="progressbar"
            aria-valuenow={(props.quantity/10)*100}
            aria-valuemin="0"
            aria-valuemax="100"
            style={{
              width: `${(props.quantity/10)*100}%`,
            }}
          />
        </div>
        <UpdateItem type={props.type} language={props.i18n.language} />
      </div>
      {/*
        same as above but add 'progress-bar-danger' after 'progress-bar' for item below than 5 KG
        <div className="progress">
          <div
            className="progress-bar progress-bar-danger progress-bar-striped active"
            role="progressbar"
            aria-valuenow={(props.quantity/10)*100}
            aria-valuemin="0"
            aria-valuemax="100"
            style={{
              width: `${(props.quantity/10)*100}%`,
            }}
          />
        </div>
      </div>
      */}
      <div className="panel-footer">{props.i18n.strings.seller_dashboard.earnings} : {props.i18n.strings.items.currency} {parseFloat(props.quantity * props.price).toFixed(2)}</div>
    </div>
  </article>
)

class SellerDashboard extends React.Component {
  constructor(props) {
    super(props)
    this.i18n = new I18n(this.props.language)
    this.loadPromise = {}
    //this.user = {}
    this.state = {
      uid: '',
      user: {},
      total: 0,
      alert: '',
      referCopy: '',
    }
  }

  componentDidMount() {
    const CopyToClipboard = require('react-copy-to-clipboard')
    //const reactGA = require('../utils/googleanalytics.js')

    const user = firebase.auth().currentUser

    const getItems = ({ uid }) => {
      //reactGA.trackPage(uid)

      return new Promise((resolve, reject)=> {
      // console.log('getting items for '+uid)
      const ReferElement = (
        <CopyToClipboard text={`https://app.xused.com/${this.i18n.language}/?ref=${uid}`}>
          <Button className="copy-btn">{this.i18n.strings.profile.copy}</Button>
        </CopyToClipboard>
      )
      //console.log('after element')
      firebase.database().ref(`/users/${uid}/`)
        .on('value', (snapshot) => {
          //console.log('got user value' + uid)
          let total = 0
          const result = snapshot.val()
          if (result.items) {
            Object.keys(result.items).forEach((type) => {
              total += this.i18n.strings.items.types[type].price * result.items[type].quantity
            })
          }

          if (!result.email || !result.displayName) {
            result.email = firebase.auth().currentUser.email
            result.displayName = firebase.auth().currentUser.displayName
            result.signUpDate = new Date().toISOString()
            result.date = new Date().toISOString()
          }

          firebase.database().ref(`/users/${uid}`).update(result)
          result.uid = uid
          //console.log(result)
          let state = {}
          if (!result.items) {
            state = {
              referCopy: ReferElement,
              alert: <UpdateAllItems language={this.i18n.language} />,
              user: result,
              total,
            }
          } else {
            state = { referCopy: ReferElement, user: result, total }
            // this.user = result
            // this.total = total
            // this.referCopy = ''
          }
          this.setState(state)
          resolve(state)
        })
        }).catch(error => console.log(error))
    }

    if (user) {
      //console.log('has user ', user)
      this.loadPromise = getItems({ uid: user.uid }).then(r => this.setState(r))
    } else {
      this.loadPromise = new Promise(
        resolve => firebase.auth().onAuthStateChanged(u => resolve(u)))
      this.loadPromise.then(getItems)
      // FIXME: isn't viable because then we lose updating user quanties
      .then(r => this.setState(r))
    }
  }

  componentWillUnmount() {
    console.log('cancelling promise')
    //this.loadPromise.cancel()
  }

  getQuantity(key) {
    if (this.state.user) {
      if (this.state.user.items) {
        if (this.state.user.items[key]) {
          return this.state.user.items[key].quantity
        }
      }
    }
    return 0
  }

  openSellAlert() {
    let alert = (
      <SweetAlert
        type="success"
        title={this.i18n.strings.seller_dashboard.modal_title}
        content={this.i18n.strings.seller_dashboard.modal_content}
        onConfirm={e => this.confirmSell(e)}
      />
    )
    if (this.state.user.warnCountry) {
      alert = (<WarnCountry i18n={this.i18n} cont={() => this.setState({ alert: '' })} />)
    }
    const confirmAlert = (
      <SweetAlert
        showCancel
        type="warning"
        title={this.i18n.strings.seller_dashboard.review_title}
        content={this.i18n.strings.seller_dashboard.review_content}
        cancelBtnText={this.i18n.strings.seller_dashboard.review_btn}
        cancelBtnBsStyle="warning"
        confirmBtnText={this.i18n.strings.seller_dashboard.review_confirm_btn}
        onCancel={() => this.setState({ alert: '' })}
        onConfirm={() => this.setState({ alert })}
      />
    )
    this.setState({ alert: confirmAlert })
  }

  confirmSell() {
    const uid = firebase.auth().currentUser
    firebase.database().ref(`/users/${uid.uid}/selling`)
      .set('true')
    this.setState({ alert: '' })
  }


  render() {
    return (
      <div>
        <Helmet
          title={config.siteTitle}
          meta={[
          { name: 'description', content: this.i18n.strings.meta.description },
          { name: 'keywords', content: 'Recycle, Earn Money, Save Environment, Waste Items' },
          ]}
        />
        <Navbar routes={this.props.route} language={this.props.language} />
        <section id="list_items_seller" className="container">
          <div className="row">

            {/* Page Heading/Breadcrumbs */}
            <div className="row">
              <div className="col-lg-12">
                <h1 className="page-header">{this.i18n.strings.seller_dashboard.title}</h1>
              </div>
            </div>
            {/*  /.row */}

            {/*  List Of Items */}
            {Object.keys(this.i18n.strings.items.types)
            .filter((item) => {
              const reject = { Glass: true, Food: true }
              return !reject[item]
            })
              .map(
              (itemKey) => {
                const items = this.i18n.strings.items
                const fullItem = items.types[itemKey]
                return (
                  <WasteItem quantity={this.getQuantity(itemKey)} i18n={this.i18n} type={itemKey} key={itemKey} image={fullItem.image} price={fullItem.price} />

                )
               }
              )
            }
          </div>
          {/* <div className="input-group">
            <span className="input-group-btn">
              <Button
                className="btn btn-danger btn-number"
                data-type="minus"
                data-field=""
                >
                  <span className="glyphicon glyphicon-minus"></span>
              </Button>
            </span>
            <input
              type="text"
              name="quant[2]"
              className="form-control input-number"
              value="10"
              min="1"
              max="100"
              />
             <span className="input-group-btn">
               <Button
                 className="btn btn-success btn-number"
                 data-type="plus"
                 data-field=""
                / >
                 <span className="glyphicon glyphicon-plus"></span>
               </Button>
             </span>
          </div> */}
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">{this.i18n.strings.seller_dashboard.total_earnings}</h3>
            </div>
            <div className="panel-total-pricing panel-body">
              {this.i18n.strings.items.currency} {parseFloat(this.state.total).toFixed(2)}
            </div>
          </div>
          {/* TODO: Translate */}
          {this.state.alert}
          <Button
            id="btn-item-submit"
            className="btn col-xs-12 col-md-12"
            bsStyle="success"
            bsSize="large"
            onClick={e => this.openSellAlert(e)}
          >
            {this.i18n.strings.seller_dashboard.sell_item}
          </Button>
          <div className='row'>
            <DashboardReference i18n={this.i18n} />
            <article className="col-md-6">
              <div className="panel panel-pricing-guide panel-success">
                <div className="panel-heading">
                  <h3 className="panel-title">{this.i18n.strings.dashboard_reference.profile}</h3>
                </div>
                <div className="panel-body">
                  <table className="table">
                    <thead>
                      <th className="pricing_type" />
                      <th className="pricing_type" />
                    </thead>
                    <tbody>
                      <tr>
                        <td className="items_type_td">{this.i18n.strings.register.full_name}</td>
                        <td className="items_pricing_td"> {this.state.user.displayName}</td>
                      </tr>
                      <tr>
                        <td className="items_type_td">{this.i18n.strings.register.email}</td>
                        <td className="items_pricing_td"> {this.state.user.email}</td>
                      </tr>
                      <tr>
                        <td className="items_type_td">{this.i18n.strings.profile.phone_number}</td>
                        <td className="items_pricing_td"> {this.state.user.phone_number}</td>
                      </tr>
                      <tr>
                        <td className="items_type_td">{this.i18n.strings.profile.bank_name}</td>
                        <td className="items_pricing_td"> {this.state.user.bank_name}</td>
                       </tr>
                      <tr>
                        <td className="items_type_td">{this.i18n.strings.profile.bank_account_number}</td>
                        <td className="items_pricing_td"> {this.state.user.bank_account}</td>
                      </tr>
                      <tr>
                        <td className="items_type_td">{this.i18n.strings.profile.city}</td>
                        <td className="items_pricing_td"> {this.state.user.city}</td>
                      </tr>
                      <tr>
                        <td className="items_type_td">{this.i18n.strings.profile.state}</td>
                        <td className="items_pricing_td"> {this.state.user.state}</td>
                      </tr>
                      <tr>
                        <td className="items_type_td">{this.i18n.strings.profile.country}</td>
                        <td className="items_pricing_td"> {this.state.user.country}</td>
                      </tr>

                      <tr>
                        <td colSpan="4">
                          <Button id="referral-btn"
                            onClick={e =>
                              this.setState({
                                alert: (
                                  <SweetAlert
                                    title={this.i18n.strings.profile.referrer_title}
                                    content={
                                      <div>
                                        <ShareButtons.FacebookShareButton
                                          url={`https://app.xused.com/${this.i18n.language}/?ref=${this.state.user.uid}`}
                                          title={this.i18n.strings.profile.fb_title}
                                          description={this.i18n.strings.profile.fb_desc}
                                          className="fb_share"
                                        >
                                          <FacebookIcon
                                            size={64}
                                            round
                                          />
                                        </ShareButtons.FacebookShareButton>
                                        <ShareButtons.TwitterShareButton
                                          url={`https://app.xused.com/${this.i18n.language}/?ref=${this.state.user.uid}`}
                                          title={this.i18n.strings.profile.twitter_title}
                                          description={this.i18n.strings.profile.twitter_title}
                                          className="twitter_share"
                                        >
                                          <TwitterIcon
                                            size={64}
                                            round
                                          />
                                        </ShareButtons.TwitterShareButton>
                                        {this.state.user.uid}
                                        {this.state.referCopy}
                                      </div>}
                                    onConfirm={e => this.setState({ alert: '' })}
                                  />
                                ),
                              })}
                          >
                            {this.i18n.strings.profile.referrer_button}
                          </Button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </article>
          </div>
        </section>
        <Footer language={this.props.language} />
      </div>
    )
  }
}

SellerDashboard.propTypes = {
  language: React.PropTypes.string
}

export default SellerDashboard
