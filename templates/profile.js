import React from 'react'
import Helmet from 'react-helmet'
import { config } from 'config'
//import { CountryDropdown, RegionDropdown } from 'react-country-region-selector'
import SweetAlert from 'react-bootstrap-sweetalert'
import I18n from '../utils/i18n.js'
import '../css/main.css'
import Navbar from './navbar'
import Footer from './footer'
import firebase from '../utils/firebase.js'
import WarnCountry from './warncountry'
//import Geocoder from 'react-select-geocoder'

import '../node_modules/react-select/dist/react-select.css'

class Profile extends React.Component {
  constructor(props) {
    super(props)
    this.i18n = new I18n(props.language)
    this.user = {}
    this.state = {
      city: '',
      state: '',
      country: 'Malaysia',
      warnCountry: '',
      phone_number: '',
      bank_account: '',
      bank_name: '',
      referrer: '',
      geo: '',
      deleteModal: '',
      // FIXME: Should only update when actually new user
      signUpDate: new Date().toISOString(),
    }
  }

  componentDidMount() {
    const Geocoder = require('react-select-geocoder')

    firebase.auth().onAuthStateChanged( (user) => {
      this.user = user
      this.setState({
        referrer: localStorage.getItem('refer') || '',
        geo: (
          <Geocoder
            apiKey="mapzen-eM7Nxyd"
            onChange={e => this.setState(
              {
                country: e.properties.country,
                state: e.properties.region,
                city: e.properties.name,
              }
            )}
          />
        ),
      })
    })

    const user = firebase.auth().currentUser

    const getProfile = ({ uid }) => {
      firebase.database().ref(`/users/${uid}`).once('value')
      .then(profileSnap => this.setState(profileSnap.val()))
    }

    if (user) {
      getProfile(user)
    } else {
      firebase.auth().onAuthStateChanged(getProfile)
    }
  }

  changeToBuyer() {
    const currentProfile = this.state
    delete currentProfile.geo
    delete currentProfile.deleteModal
    currentProfile.from_seller = true

    firebase.database().ref(`buyers/${this.user.uid}`).set(currentProfile)
    .then(() => this.props.history.push(this.i18n.link('buyer_profile')))
  }

  deleteAccount() {
    const currentProfile = this.state
    delete currentProfile.geo
    delete currentProfile.deleteModal
    currentProfile.deleteDate = new Date()
    console.log(currentProfile)
    firebase.database().ref(`deletedAccounts/${this.user.uid}`).set(currentProfile)
    firebase.database().ref(`users/${this.user.uid}`).remove()
    firebase.auth().currentUser.delete().then(() => this.props.history.push(''))
  }

  showDeleteModal() {
    this.setState({
      deleteModal: (
        <SweetAlert
          title={this.i18n.strings.profile.delete_modal_title}
          content={
            <div>{this.i18n.strings.profile.delete_modal_header}<br />
              <div>
                <button onClick={e => this.deleteAccount()} className="btn btn-delete-profile btn-danger">{this.i18n.strings.profile.delete_modal}</button>
                <button onClick={e => this.changeToBuyer()} className="btn btn-switch-profile btn-success">{this.i18n.strings.buyer_profile.switch_modal}</button>
              </div>
            </div>}
          type="warning"
          confirmBtnText="Cancel"
          confirmBtnBsStyle="default"
          onConfirm={() => this.setState({ deleteModal: '' })}
        />
      ),
    })
  }

  submit(e) {
    e.preventDefault()
    if (!this.state.country || !this.state.state || !this.state.city) {
      alert('Please ensure that you have searched for a city that fills all the input boxes')
      return
    }
    const cont = (safe) => {
      this.setState({ warnCountry: '' })
      const document = this.state
      document.warnCountry = !safe
      delete document.geo
      firebase.database().ref(`/users/${firebase.auth().currentUser.uid}`).update(document)
      this.props.history.push(this.i18n.link('seller_dashboard'))
    }
    // FIXME: Validation
    if (['Malaysia', 'Indonesia'].indexOf(this.state.country) < 0) {
      this.setState({ warnCountry: (<WarnCountry i18n={this.i18n} cont={cont} />),
      })
    } else {
      cont(true)
    }
  }

  handleChange(target) {
    return (event) => {
      const key = {}
      key[target] = event.target.value
      this.setState(key)
    }
  }

  render() {
    return (
      <div>
        {this.state.warnCountry}
        <Helmet
          title={config.siteTitle}
          meta={[
            { name: 'description', content: this.i18n.strings.meta.description },
            { name: 'keywords', content: 'Recycle, Earn Money, Save Environment, Waste Items' },
          ]}
        />
        <Navbar routes={this.props.route} language={this.props.language} />
        <div className="container">
          <div className="col-sm-6 col-sm-offset-3">
            <h1 className="register-header">{this.i18n.strings.profile.header}</h1>
            <form onSubmit={(e) => { this.submit(e) }} >
              <div className="form-group">
                <label htmlFor="search">{this.i18n.strings.profile.search}</label>
                {this.state.geo}
                {/* <Geocoder
                  apiKey="mapzen-eM7Nxyd"
                  onChange={e => this.setState(
                    {
                      country: e.properties.country,
                      state: e.properties.region,
                      city: e.properties.name,
                    }
                  )}
                /> */}
              </div>
              <div className="form-group">
                <label htmlFor="address">{this.i18n.strings.profile.country}</label>
                {/* <CountryDropdown
                  classes="form-control"
                  value={this.state.country}
                  onChange={val => this.setState({ country: val })}
                /> */}
                <input
                  id="address"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  readOnly
                  value={this.state.country}
                  placeholder={this.i18n.strings.profile.country_p}
                />
              </div>
              <div className="form-group">
                <label htmlFor="address">{this.i18n.strings.profile.state}</label>
                {/*
                <RegionDropdown
                  classes="form-control"
                  value={this.state.state}
                  onChange={val => this.setState({ state: val })}
                  country={this.state.country}
                />
                */}
                <input
                  id="address"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  readOnly
                  required
                  value={this.state.state}
                  placeholder={this.i18n.strings.profile.state_p}
                />
              </div>
              <div className="form-group">
                <label htmlFor="address">{this.i18n.strings.profile.city}</label>
                <input
                  id="address"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  value={this.state.city}
                  required
                  onChange={this.handleChange('city')}
                  placeholder={this.i18n.strings.profile.city_p}
                />
              </div>
              <div className="form-group">
                <label htmlFor="phone_number">{this.i18n.strings.profile.phone_number}</label>
                <input
                  id="phone_number"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  required
                  value={this.state.phone_number}
                  onChange={this.handleChange('phone_number')}
                  placeholder={this.i18n.strings.profile.phone_number}
                />
              </div>
              <div className="form-group">
                <label htmlFor="bank_account">{this.i18n.strings.profile.bank_name}</label>
                <input
                  id="bank_name"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  value={this.state.bank_name}
                  onChange={this.handleChange('bank_name')}
                  placeholder={this.i18n.strings.profile.bank_name_placeholder}
                />
              </div>
              <div className="form-group">
                <label htmlFor="bank_account">{this.i18n.strings.profile.bank_account_number}</label>
                <input
                  id="bank_account"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  value={this.state.bank_account}
                  onChange={this.handleChange('bank_account')}
                  placeholder={this.i18n.strings.profile.bank_account_number_placeholder}
                />
              </div>
              <div className="form-group">
                <label htmlFor="bank_account">{this.i18n.strings.profile.referrer_code}</label>
                <input
                  id="bank_account"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  value={this.state.referrer}
                  onChange={this.handleChange('referrer')}
                  placeholder={this.i18n.strings.profile.referrer_code_placeholder}
                />
              </div>

              <button type="submit" className="btn btn-register btn-success">
                {this.i18n.strings.profile.submit}
              </button>
              <hr></hr>
            </form>
            {this.state.deleteModal}
            <button
              className="btn btn-delete btn-danger"
              onClick={() => this.showDeleteModal()}
            >
              {this.i18n.strings.buyer_profile.delete_btn}
            </button>
          </div>
        </div>
        <Footer language={this.props.language} />
      </div>
    )
  }
}

export default Profile
