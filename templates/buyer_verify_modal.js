import React from 'react'
import SweetAlert from 'react-bootstrap-sweetalert'

export default props => (<SweetAlert
  type="warning"
  content={props.i18n.strings.buyer_dashboard.verify_alert}
  disabled="true"
  confirmBtnText={props.i18n.strings.buyer_dashboard.verify_button}
  onConfirm={e => props.history.push(props.i18n.link(''))}
/>)
