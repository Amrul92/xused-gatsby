import React from 'react'
import Helmet from 'react-helmet'

import { config } from 'config'

import I18n from '../utils/i18n.js'
import Navbar from './navbar'
import Footer from './footer'
import '../css/main.css'
import about from '../images/about_us.jpg'

const About = props => {
  const i18n = new I18n(props.language)
  return (
    <div>
      <Helmet
        title={config.siteTitle}
        meta={[
        { name: 'description', content: i18n.strings.meta.description },
        { name: 'keywords', content: 'Recycle, Earn Money, Save Environment, Waste Items' },
        ]}
      />
      <Navbar routes={props.route} language={props.language} />
      {/* Content */}
      <div className="container">

        {/* Page Heading/Breadcrumbs  */}
        <div className="row">
          <div className="col-lg-12">
            <h1 className="page-header">{i18n.strings.about.title}</h1>
          </div>
        </div>

        {/* Intro Content */}
        <div className="row">
          <div className="col-md-6">
            <div id="aboutimg" className="img-responsive about_img" />
          </div>
          <div className="col-md-6">
            <h2>{i18n.strings.about.how}</h2>
            <p>{i18n.strings.about.p1}</p><br />
            <p>{i18n.strings.about.p2}</p>
          </div>
        </div>
      </div>
      <Footer language={props.language} />
    </div>
  )
}

About.propTypes = {
  language: React.PropTypes.string,
}

export default About
