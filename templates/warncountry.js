import React from 'react'
import SweetAlert from 'react-bootstrap-sweetalert'

const warnCountry = props => (
  <SweetAlert
    confirmBtnText="I understand."
    type="warning"
    title="Thanks for your interest!"
    content={props.i18n.strings.register.notify_header}
    onConfirm={props.cont}
  />
)

warnCountry.propTypes = {
  i18n: React.PropTypes.function,
  cont: React.PropTypes.function,
}

export default warnCountry
