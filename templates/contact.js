import React from 'react'
import Helmet from 'react-helmet'
import Icon from 'react-fa'

import I18n from '../utils/i18n.js'
import '../css/main.css'
import Navbar from './navbar.js'
import Footer from './footer.js'

const Contact = (props) => {
  // TODO: Helmet Metadata
  const i18n = new I18n(props.language).strings.contact
  return (
    <div>
      <Navbar routes={props.route} language={props.language} />
      <div className="container">
        {/* Page Heading/Breadcrumbs */}
        <div className="row">
          <div className="col-lg-12">
            <h1 className="page-header">
              {i18n.contact}
            </h1>
          </div>
        </div>
        {/* /.row */}

        {/* Content Row */}
        <div className="row">
          {/* Map Column */}
          <div className="contact_us col-md-8">
            {/* Embedded Google Map */}

          </div>
          {/* Contact Details Column */}
          <div className="col-md-4">
            <h3>{i18n.hq}</h3>
            <p>
              {i18n.addr1}<br />{i18n.addr2}<br />
            </p>
            <p>
              <Icon name="envelope-o" />
              {/* TODO: implement non-spam email code */}
              <abbr title="Email">E</abbr>: <a href="mailto:hello@xused.com">hello@xused.com</a>
            </p>
          </div>
        </div>
      </div>
      <Footer language={props.language} />
    </div>
  )
}

Contact.propTypes = {
  language: React.PropTypes.string,
}

export default Contact
