import React from 'react'
import firebase from '../utils/firebase.js'
import Helmet from 'react-helmet'
import I18n from '../utils/i18n.js'
import '../css/main.css'
import Navbar from './navbar'
import Footer from './footer'
import {
  config,
} from 'config'


class Login extends React.Component {
  constructor(props) {
    super(props)
    this.i18n = new I18n(this.props.language)
    this.state = {
      email: '',
      password: '',
      error: '',
      user: '',
    }
  }

  componentDidMount() {
    const reactGA = require('../utils/googleanalytics')

    reactGA.trackPage()

    // auto redirect if already logged in?
    firebase.auth().onAuthStateChanged(user => this.setState({ user }))
  }

  handleSubmit(e) {
    e.preventDefault()

    const email = this.state.email
    const pw = this.state.password
    firebase.auth().signInWithEmailAndPassword(email, pw).then((result) => {
      firebase.database().ref(`/buyers/${result.uid}/`).once('value').then(buyerSnap =>{
        if (buyerSnap.val()) {
          this.props.history.push(this.i18n.link('buyer_profile'))
        } else {
          // this line will redirect correctly once the buyer dashboard is finalized
          this.props.history.push(this.i18n.link(`${buyerSnap.val() ? 'buyer_' : 'seller_'}dashboard`))
        }
      })
    }).catch((error) => {
      this.setState({ error })
    })
  }

  render() {
    return (
      <div>
        <Helmet
          title={config.siteTitle}
          meta={[
          { name: 'description', content: this.i18n.strings.meta.description },
          { name: 'keywords', content: 'Recycle, Earn Money, Save Environment, Waste Items' },
          ]}
        />
        <Navbar routes={this.props.route} language={this.props.language} />
        <div className="container">
          <div className="col-sm-6 col-sm-offset-3">
            <h1 className="register-header">{this.i18n.strings.login.login_header}</h1>
            <form onSubmit={e => this.handleSubmit(e)}>
              <div className="form-group">
                <label htmlFor="email">{this.i18n.strings.login.email}</label>
                <input
                  id="email"
                  type="email"
                  className="form-control"
                  onChange={i => this.setState({ email: i.target.value })}
                  value={this.state.email}
                  placeholder={this.i18n.strings.login.email}
                />
              </div>
              <div className="form-group">
                <label htmlFor="pw">{this.i18n.strings.login.password}</label>
                <input
                  id="pw"
                  type="password"
                  className="form-control"
                  onChange={i => this.setState({ password: i.target.value })}
                  value={this.state.password}
                  placeholder={this.i18n.strings.login.password}
                />
              </div>

              <div>{this.state.error.message}</div>
              <button type="submit" className="btn btn-login btn-success">{this.i18n.strings.login.submit}</button>
            </form>
            {/* TODO: Translate error keys instead of showing raw message */}
          </div>
        </div>
        <Footer language={this.props.language} />
      </div>
    )
  }
}

Login.propTypes = {
  language: React.PropTypes.string,
}

export default Login
