import React from 'react'
import { Popover, Tooltip, Modal, Button, OverlayTrigger, FieldGroup, Radio, FormControl } from 'react-bootstrap'
import I18n from '../utils/i18n'
import firebase from '../utils/firebase'


const UpdateItem = React.createClass({
  getInitialState() {
    return {
      showModal: false,
      quantity: 0,
      selectedItem: 'Used Cooking Oil',
      selectedPlace: 'Subang Jaya',
    };
  },

  handleChange(field) {
    return (event) => {
      const change = {}
      const value = parseFloat(event.target.value)
      if (value < 0) {
        alert("Please enter a valid number")
      } else {
        change[field] = value
        this.setState(change)
      }
    }
  },

  close() {
    this.setState({ showModal: false });
  },

  submit(e) {
    e.preventDefault()
    const uid = firebase.auth().currentUser.uid
    if (!uid) {
      alert('not logged in!')
    }
    const document = {
      type: this.props.type,
      quantity: this.state.quantity,
    }
    firebase.database()
      .ref(`/users/${uid}/items/${document.type}`)
      .set(document)
    firebase.database()
      .ref(`/users/${uid}`)
      .update({ date: new Date().toISOString() })
    firebase.database()
      .ref(`/users/${uid}/updates`)
    // can use date as key, but do we want to?
      .push({ date: new Date().toISOString(), type: document.type, quantity: document.quantity })
    this.close()
  },

  open() {
    this.setState({ showModal: true });
  },

  itemRadioChange(e) {
    this.setState({
      selectedItem: e.target.value,
    })
  },

  placeRadioChange(e) {
    this.setState({
      selectedPlace: e.target.value,
    })
  },

  render() {
    const i18n = new I18n(this.props.language)

    const popover = (
      <Popover id="modal-popover" title="popover">
        very popover. such engagement
      </Popover>
    );
    const tooltip = (
      <Tooltip id="modal-tooltip">
        wow.
      </Tooltip>
    );

    return (
      <div>

        <Button
          id="btn-item-edit"
          className="btn btn-warning col-xs-12 col-md-12"
          bsStyle="warning"
          bsSize="large"
          onClick={this.open}
        >
          {i18n.strings.update.button}
        </Button>

        <Modal show={this.state.showModal} onHide={this.close}>
          <Modal.Header closeButton>
            <Modal.Title>{i18n.strings.update.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form onSubmit={this.submit}>
              <h4>{i18n.strings.items.types[this.props.type].type}</h4>
              <h4>{i18n.strings.update.specify}</h4>
              <FormControl
                value={this.state.quantity}
                type="number"
                placeholder={i18n.strings.update.specify_placeholder}
                onChange={this.handleChange('quantity')}
              />
            </form>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close}>{i18n.strings.update.close}</Button>
            <Button bsStyle="warning" onClick={this.submit}>{i18n.strings.update.submit}</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
});

export default UpdateItem
