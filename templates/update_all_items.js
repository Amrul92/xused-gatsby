import React from 'react'
import { Popover, Tooltip, Modal, Button, FormControl, FormGroup } from 'react-bootstrap'
import I18n from '../utils/i18n'
import firebase from '../utils/firebase'

/*
function FieldGroup({ id, label, help, ...props }) {
  return (
    <FormGroup controlId={id}>
      <ControlLabel>{label}</ControlLabel>
      <FormControl {...props} />
      {help && <HelpBlock>{help}</HelpBlock>}
    </FormGroup>
  );
}
*/

const UpdateItem = React.createClass({
  getInitialState() {
    return {
      showModal: true,
      quantities: {
        'Used Cooking Oil': 0,
        Cans: 0,
        Plastic: 0,
        Paper: 0,
      },
      selectedItem: 'Used Cooking Oil',
      selectedPlace: 'Subang Jaya',
    };
  },

  handleChange(field) {
    return (event) => {
      const change = {}
      const value = parseFloat(event.target.value)
      if (value < 0) {
        alert("Please enter a valid number")
      } else {
        change[field] = value
        this.setState(change)
      }
    }
  },

  close() {
    this.setState({ showModal: false });
  },

  submit(e) {
    e.preventDefault()
    const uid = firebase.auth().currentUser.uid
    if (!uid) {
      alert('not logged in!')
    }
    const document = {
      Paper:{quantity: this.state.quantity_paper || 0},
      'Used Cooking Oil':{quantity: this.state.quantity_uco || 0},
      Cans:{quantity: this.state.quantity_cans || 0},
      Plastic:{quantity: this.state.quantity_plastic || 0},
    }
    firebase.database()
      .ref(`/users/${uid}/items/`)
      .set(document)
    firebase.database()
      .ref(`/users/${uid}`)
      .update({
        date: new Date().toISOString(),
        initialUpdate: true,
      })
    this.close()
  },

  open() {
    this.setState({ showModal: true });
  },

  render() {
    const i18n = new I18n(this.props.language)

    const popover = (
      <Popover id="modal-popover" title="popover">
      </Popover>
    );
    const tooltip = (
      <Tooltip id="modal-tooltip">
      </Tooltip>
    );

    return (
      <div>
        <Modal show={this.state.showModal} onHide={this.close}>
          <Modal.Header closeButton>
            <Modal.Title>{i18n.strings.update.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form onSubmit={this.submit}>

              {/*
              <Radio
                value="Used Cooking Oil"
                onChange={this.itemRadioChange}
                checked={this.state.selectedItem === 'Used Cooking Oil'}
              >
                {i18n.strings.update.uco}
              </Radio>
              <Radio
                value="Cans"
                onChange={this.itemRadioChange}
                checked={this.state.selectedItem === 'Cans'}
              >
                {i18n.strings.update.cans}
              </Radio>
              <Radio
                value="Plastic"
                onChange={this.itemRadioChange}
                checked={this.state.selectedItem === 'Plastic'}
              >
                {i18n.strings.update.plastic}
              </Radio>
              <Radio
                value="Paper"
                onChange={this.itemRadioChange}
                checked={this.state.selectedItem === 'Paper'}
              >
                {i18n.strings.update.paper}
              </Radio>
              */}
              <h3>{i18n.strings.update.update_all_header}</h3>
              <h4>{i18n.strings.update.specify}</h4>
              <FormGroup>
                <FormControl
                  value={this.state.quantity_uco}
                  type="number"
                  placeholder={i18n.strings.items.types['Used Cooking Oil'].type}
                  onChange={this.handleChange('quantity_uco')}
                />
              </FormGroup>
              <FormGroup>
                <FormControl
                  value={this.state.quantity_cans}
                  type="number"
                  placeholder={i18n.strings.items.types['Cans'].type}
                  onChange={this.handleChange('quantity_cans')}
                />
              </FormGroup>
              <FormGroup>
                <FormControl
                  value={this.state.quantity_plastic}
                  type="number"
                  placeholder={i18n.strings.items.types['Plastic'].type}
                  onChange={this.handleChange('quantity_plastic')}
                />
              </FormGroup>
              <FormGroup>
                <FormControl
                  value={this.state.quantity_paper}
                  type="number"
                  placeholder={i18n.strings.items.types['Paper'].type}
                  onChange={this.handleChange('quantity_paper')}
                />
              </FormGroup>
            </form>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close}>{i18n.strings.update.close}</Button>
            <Button bsStyle="warning" onClick={this.submit}>{i18n.strings.update.submit}</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
});

export default UpdateItem
