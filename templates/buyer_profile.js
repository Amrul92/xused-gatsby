import React from 'react'
import Helmet from 'react-helmet'
import { config } from 'config'
//import { CountryDropdown, RegionDropdown } from 'react-country-region-selector'
import { Button, Table, Radio } from 'react-bootstrap'

import I18n from '../utils/i18n.js'
import '../css/main.css'
import Navbar from './navbar'
import Footer from './footer'
import firebase from '../utils/firebase.js'
import WarnCountry from './warncountry'
//import Geocoder from 'react-select-geocoder'
import BuyerVerifyModal from './buyer_verify_modal.js'

import '../node_modules/react-select/dist/react-select.css'

class Profile extends React.Component {
  constructor(props) {
    super(props)
    this.i18n = new I18n(props.language)
    this.state = {
      city: '',
      state: '',
      country: 'Malaysia',
      interest_type: 'Used Cooking Oil',
      warnCountry: '',
      phone_number: '',
      bank_account: '',
      bank_name: '',
      referrer: '',
      geo: '',
      // FIXME: Should only update when actually new user
      signUpDate: new Date().toISOString(),
    }
  }

  componentDidMount() {
    const Geocoder = require('react-select-geocoder')
    this.setState({
      referrer: localStorage.getItem('refer') || '',
      geo: (
        <Geocoder
          apiKey="mapzen-eM7Nxyd"
          onChange={e => this.setState(
            {
              country: e.properties.country,
              state: e.properties.region,
              city: e.properties.name,
            }
            )}
        />
      ),
      //displayName: firebase.auth().currentUser.displayName,
      //email: firebase.auth().currentUser.email,
    })
    const user = firebase.auth().currentUser

    const getProfile = ({ uid }) => {
      firebase.database().ref(`/buyers/${uid}`).once('value')
      .then(profileSnap => this.setState(profileSnap.val()))
    }

    if (user) {
      getProfile(user)
    } else {
      firebase.auth().onAuthStateChanged(getProfile)
    }
  }

  itemRadioChange(e) {
    this.setState({
      interest_type: e.target.value,
    })
  }

  submit(e) {
    e.preventDefault()
    if (!this.state.country || !this.state.state || !this.state.city || !this.state.phone_number) {
      alert('Please ensure that you have searched for a city that fills all the input boxes')
      return
    }
    const cont = (safe) => {
      this.setState({ warnCountry: '' })
      const document = this.state
      document.warnCountry = !safe
      delete document.geo
      firebase.database().ref(`/buyers/${firebase.auth().currentUser.uid}`).update(document)
      this.setState({ warnCountry:  <BuyerVerifyModal i18n={this.i18n} history={this.props.history}/> })
      // Redirect once dashboard is up
      // this.props.history.push(this.i18n.link('buyer_dashboard'))
    }
    // FIXME: Validation
    if (['Malaysia', 'Indonesia'].indexOf(this.state.country) < 0) {
      this.setState({ warnCountry: (<WarnCountry i18n={this.i18n} cont={cont} />),
      })
    } else {
      cont(true)
    }
  }

  handleChange(target) {
    return (event) => {
      const key = {}
      key[target] = event.target.value
      this.setState(key)
    }
  }

  render() {
    return (
      <div>
        {this.state.warnCountry}
        <Helmet
          title={config.siteTitle}
          meta={[
            { name: 'description', content: this.i18n.strings.meta.description },
            { name: 'keywords', content: 'Recycle, Earn Money, Save Environment, Waste Items' },
          ]}
        />
        <Navbar routes={this.props.route} language={this.props.language} />
        <div className="container">
          <div className="col-sm-6 col-sm-offset-3">
            <h1 className="register-header">{this.i18n.strings.buyer_profile.header}</h1>
            <form onSubmit={(e) => { this.submit(e) }} >
              <div className="form-group">
                <label htmlFor="address">{this.i18n.strings.profile.items_type}</label>
                <Radio
                  value="Used Cooking Oil"
                  onChange={e => this.itemRadioChange(e)}
                  checked={this.state.interest_type === 'Used Cooking Oil'}
                >
                  {this.i18n.strings.update.uco}
                </Radio>
                <Radio
                  value="Cans"
                  onChange={e => this.itemRadioChange(e)}
                  checked={this.state.interest_type === 'Cans'}
                >
                  {this.i18n.strings.update.cans}
                </Radio>
                <Radio
                  value="Paper"
                  onChange={e => this.itemRadioChange(e)}
                  checked={this.state.interest_type === 'Paper'}
                >
                  {this.i18n.strings.update.paper}
                </Radio>
                <Radio
                  value="Plastic"
                  onChange={e => this.itemRadioChange(e)}
                  checked={this.state.interest_type === 'Plastic'}
                >
                  {this.i18n.strings.update.plastic}
                </Radio>
                <label htmlFor="search">{this.i18n.strings.profile.search}</label>
                {this.state.geo}
                {/* <Geocoder
                  apiKey="mapzen-eM7Nxyd"
                  onChange={e => this.setState(
                    {
                      country: e.properties.country,
                      state: e.properties.region,
                      city: e.properties.name,
                    }
                  )}
                /> */}
              </div>
              <div className="form-group">
                <label htmlFor="address">{this.i18n.strings.profile.country}</label>
                {/* <CountryDropdown
                  classes="form-control"
                  value={this.state.country}
                  onChange={val => this.setState({ country: val })}
                /> */}
                <input
                  id="address"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  readOnly
                  required
                  value={this.state.country}
                  placeholder={this.i18n.strings.profile.country_p}
                />
              </div>
              <div className="form-group">
                <label htmlFor="address">{this.i18n.strings.profile.state}</label>
                {/*
                <RegionDropdown
                  classes="form-control"
                  value={this.state.state}
                  onChange={val => this.setState({ state: val })}
                  country={this.state.country}
                />
                */}
                <input
                  id="address"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  readOnly
                  required
                  value={this.state.state}
                  placeholder={this.i18n.strings.profile.state_p}
                />
              </div>
              <div className="form-group">
                <label htmlFor="address">{this.i18n.strings.profile.city}</label>
                <input
                  id="address"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  required
                  value={this.state.city}
                  onChange={this.handleChange('city')}
                  placeholder={this.i18n.strings.profile.city_p}
                />
              </div>
              <div className="form-group">
                <label htmlFor="phone_number">{this.i18n.strings.profile.phone_number}</label>
                <input
                  id="phone_number"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  required
                  value={this.state.phone_number}
                  onChange={this.handleChange('phone_number')}
                  placeholder={this.i18n.strings.profile.phone_number}
                />
              </div>
              <div className="form-group">
                <label htmlFor="bank_account">{this.i18n.strings.profile.bank_name}</label>
                <input
                  id="bank_name"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  value={this.state.bank_name}
                  onChange={this.handleChange('bank_name')}
                  placeholder={this.i18n.strings.profile.bank_name_placeholder}
                />
              </div>
              <div className="form-group">
                <label htmlFor="bank_account">{this.i18n.strings.profile.bank_account_number}</label>
                <input
                  id="bank_account"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  value={this.state.bank_account}
                  onChange={this.handleChange('bank_account')}
                  placeholder={this.i18n.strings.profile.bank_account_number_placeholder}
                />
              </div>
              <div className="form-group">
                <label htmlFor="bank_account">{this.i18n.strings.profile.referrer_code}</label>
                <input
                  id="bank_account"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  value={this.state.referrer}
                  onChange={this.handleChange('referrer')}
                  placeholder={this.i18n.strings.profile.referrer_code_placeholder}
                />
              </div>
              <hr />
              <h1 className="register-header">{this.i18n.strings.buyer_profile.header_extra}</h1>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.business_name}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('company_type')}
                  value={this.state.company_type}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.business_type}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('company_status')}
                  value={this.state.company_status}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.company_status}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('business_registration')}
                  value={this.state.business_registration}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.business_reg}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('business_url')}
                  value={this.state.business_url}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.business_url}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('business_phone')}
                  value={this.state.business_phone}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.business_phone}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('business_fax')}
                  value={this.state.business_fax}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.business_fax}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('business_email')}
                  value={this.state.business_email}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.business_email}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('business_address')}
                  value={this.state.business_address}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.business_address}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('business_type')}
                  value={this.state.business_type}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.business_establish}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('business_date_established')}
                  value={this.state.business_date_established}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.business_gst}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('business_gst')}
                  value={this.state.business_gst}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.total_staff}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('total_staff')}
                  value={this.state.total_staff}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.contact_person}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('contact_person')}
                  value={this.state.contact_person}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.contact_designation}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('contact_designation')}
                  value={this.state.contact_designation}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.contact_phone}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('contact_phone')}
                  value={this.state.contact_phone}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.contact_email}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('contact_email')}
                  value={this.state.contact_email}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.preferred_lang}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('preferred_language')}
                  value={this.state.preferred_language}
                />
              </div>
              <div className="form-group">
                <label htmlFor="type_company">{this.i18n.strings.buyer_profile.max_quantity}</label>
                <input
                  id="type_company"
                  className="form-control"
                  type="textarea"
                  label="textarea"
                  onChange={this.handleChange('max_quantity')}
                  value={this.state.max_quantity}
                />
              </div>
              <button type="submit" className="btn btn-register btn-success">
                {this.i18n.strings.profile.submit}
              </button>
            </form>
          </div>
        </div>
        <Footer language={this.props.language} />
      </div>
    )
  }
}

export default Profile
