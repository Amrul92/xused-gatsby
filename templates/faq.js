import React from 'react'
import Helmet from 'react-helmet'

import { config } from 'config'
import { Accordion, Panel } from 'react-bootstrap'

import I18n from '../utils/i18n.js'
import Footer from './footer.js'
import Navbar from './navbar.js'

import '../css/main.css'

const FAQ = (props) => {
  const i18n = new I18n(props.language)
  const itemElements = i18n.strings.faq.items
  .map(item =>
    <Panel key={item.title} header={item.title} eventKey={item.title}>
      {item.content}
    </Panel>)
  return (
    <div>
      <Helmet
        title={config.siteTitle}
        meta={[
        { name: 'description', content: i18n.strings.meta.description },
        { name: 'keywords', content: 'Recycle, Earn Money, Save Environment, Waste Items' },
        ]}
      />
      <Navbar routes={props.route} language={props.language} />
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <h1 className="page-header">
              {i18n.strings.faq.title}
            </h1>
          </div>
        </div>
        <Accordion>
          {itemElements}
        </Accordion>
      </div>
      <Footer language={props.language} />
    </div>
  )
}

FAQ.propTypes = {
  language: React.PropTypes.string,
}

export default FAQ
