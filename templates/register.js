import React from 'react';
import firebase from '../utils/firebase.js'
import Helmet from 'react-helmet'
import I18n from '../utils/i18n.js'
import '../css/main.css'
import Navbar from './navbar'
import Footer from './footer'
import {
  config,
} from 'config'
import { Link } from 'react-router'
import { Button, Jumbotron, Form, FormGroup, ControlLabel, Well, Radio } from 'react-bootstrap'

/* contextTypes: {
  router: React.PropTypes.object.isRequired
},*/

class Register extends React.Component {
  constructor(props) {
    super(props)
    this.i18n = new I18n(this.props.language)
    this.state = {
      error: '',
      fullname: '',
      email: '',
      pw: '',
      seller: true,
    }
  }

  componentDidMount() {
    const reactGA = require('../utils/googleanalytics.js')

    reactGA.trackPage()
  }

  submit(e) {
    e.preventDefault();
    const fullname = this.state.fullname;
    const email = this.state.email;
    const pw = this.state.pw;
    firebase.auth().createUserWithEmailAndPassword(email, pw)
      .then((result) => {
        result.updateProfile({
          displayName: fullname,
        }).then(() => {
          result.sendEmailVerification()
        })
        if (this.state.seller) {
          firebase.database().ref(`/users/${result.uid}`).set({
            displayName: fullname,
            email: result.email,
            // FIXME: Wrap for build?
            // referrer: localStorage.refer,
            signUpDate: new Date().toISOString(),
          })
          this.props.history.push(this.i18n.link('profile'))
        } else {
          firebase.database().ref(`/buyers/${result.uid}`).set({
            displayName: fullname,
            email: result.email,
            // FIXME: Wrap for build?
            // referrer: localStorage.refer,
            signUpDate: new Date().toISOString(),
          })
          this.props.history.push(this.i18n.link('buyer_profile'))
        }
      })
    // TODO: translate into other languages
    .catch(e => this.setState({ error: e.message }));
  }

  handleChange(target) {
    return (event) => {
      const key = {}
      key[target] = event.target.value
      this.setState(key)
    }
  }

  render() {
    const errors = this.state.error ? <p> {this.state.error} </p> : '';
    return (
      <div>
        <Helmet
          title={config.siteTitle}
          meta={[
          { name: 'description', content: this.i18n.strings.meta.description },
          { name: 'keywords', content: 'Recycle, Earn Money, Save Environment, Waste Items' },
          ]}
        />
        <Navbar routes={this.props.route} language={this.props.language} />
        <div className="container">
          <div className="col-sm-6 col-sm-offset-3">
            <h1 className="register-header"> {this.i18n.strings.register.register_header} </h1>
            <form onSubmit={(e) => { this.submit(e) }} >
              <div className="form-group">
                <label htmlFor="registration_type" className="registration_type">{this.i18n.strings.register.register_radio} : </label>
                <Radio
                  checked={this.state.seller}
                  className="type_registration_seller"
                  onClick={e => this.setState({ seller: true })}
                  inline
                >
                  {this.i18n.strings.register.register_radio_seller}
                </Radio>
                <Radio
                  checked={!this.state.seller}
                  className="type_registration_buyer"
                  onClick={e => this.setState({ seller: false })}
                  inline
                >
                  {this.i18n.strings.register.register_radio_buyer}
                </Radio>
              </div>
              <div className="form-group">
                <label htmlFor="fullname"> {this.i18n.strings.register.full_name} </label>
                <input
                  id="fullname"
                  className="form-control"
                  type="text"
                  label="Text"
                  required
                  value={this.state.fullname}
                  onChange={this.handleChange('fullname')}
                  placeholder={this.i18n.strings.register.full_name}
                />
              </div>
              <div className="form-group">
                <label htmlFor="email"> {this.i18n.strings.register.email} </label>
                <input
                  id="email"
                  className="form-control"
                  required
                  value={this.state.email}
                  onChange={this.handleChange('email')}
                  placeholder={this.i18n.strings.register.email}
                />
              </div>
              <div className="form-group">
                <label htmlFor="pw">{this.i18n.strings.register.password}</label>
                <input
                  id="pw"
                  value={this.state.pw}
                  onChange={this.handleChange('pw')}
                  required
                  type="password"
                  className="form-control"
                  placeholder={this.i18n.strings.register.password}
                />
              </div>
              {errors}
              <button type="submit" className="btn btn-register btn-success">{this.i18n.strings.register.sign_up}</button>
              <h3 className="link-login"><Link to={this.i18n.link('login')}>{this.i18n.strings.register.link_login}</Link></h3>
            </form>
          </div>
        </div>
        <Footer language={this.props.language} />
      </div>
    )
  }
}

export default Register
