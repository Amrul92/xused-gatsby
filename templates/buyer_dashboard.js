import React from 'react'
import {
  Link,
} from 'react-router'
import Helmet from 'react-helmet'
import {
  config,
} from 'config'
import { Table, Pagination, Button } from 'react-bootstrap'
import SweetAlert from 'react-bootstrap-sweetalert'

import I18n from '../utils/i18n.js'
import '../css/main.css'
import Navbar from './navbar'
import Footer from './footer'
import image from '../images/img'
import DashboardReference from './dashboard_reference'
import firebase from '../utils/firebase'
import BuyerVerifyModal from './buyer_verify_modal.js'

const WasteItem = props => (
  <article className="col-xs-6 col-sm-3 col-md-2">
    <div className="panel panel-buyer-dashboard panel-success">
      <div className="panel-heading">
        <h3 className="panel-header-buyer panel-title">{props.name}</h3>
      </div>
      <div className="panel-body">
        <div className="list_items_buyer">
          {parseFloat(props.quantity).toFixed(1)} KG
        </div>
        <Button
          id="btn-item-buyer"
          className="btn col-xs-12 col-md-12"
          bsStyle="success"
          bsSize="small"
        >
          {props.i18n.strings.buyer_dashboard.buy}
        </Button>
      </div>
      <div className="panel-footer-buyer panel-footer">{props.i18n.strings.buyer_dashboard.costs} : {props.i18n.strings.items.currency} {parseFloat(props.quantity * props.price).toFixed(2)}</div>
    </div>
  </article>
)

const SellerEntry = props => (
  <tr>
    <td>{props.key}</td>
    <td>{props.name}</td>
    <td>{props.quantity} KG</td>
  </tr>
)

class BuyerDashboard extends React.Component {
  constructor(props) {
    super(props)
    this.i18n = new I18n(this.props.language)
    this.state = {
      users: {},
      buyer: {},
      cityTotals: {},
      type: 'Loading...',
      // TODO: Amrul Translate
      verified: <BuyerVerifyModal i18n={this.i18n} history={this.props.history} />,
      price: 1,
    }
  }

  componentDidMount() {
    const user = firebase.auth().currentUser

    const getDashData = ({ uid }) => {
      firebase.database().ref(`/buyers/${uid}/`).on('value', (buyerSnapshot) => {
        const buyer = buyerSnapshot.val()
        this.setState({ buyer, type: this.i18n.strings.items.types[buyer.interest_type].type })
        firebase.database().ref('/users/')
          .on('value', snapshot => this.setState({ users: snapshot.val(), cityTotals: this.cityTotals(snapshot.val(), buyer.interest_type, buyer.state) }))
      })
    }

    if (user) {
      getDashData(user)
    } else {
      firebase.auth().onAuthStateChanged(getDashData)
    }
  }

  cityTotals(users, type, state) {
    this.setState({ price: this.i18n.strings.items.types[type].price })
    return Object.keys(users)
      .filter(userID => users[userID].items && users[userID].state === state)
      .filter(userID => (users[userID].items[type] ? users[userID].items[type].quantity : false))
      .reduce((agg, userID) => {
        agg[users[userID].city] = (agg[users[userID].city] || 0) + parseFloat(users[userID].items[type].quantity)
        return agg
      }
      , {})
    // const  = Object.keys(cities)
    //  .map(userID => users.val()[userID].items[type].quantity)
    //  .reduce((result, element) => parseFloat(result) + parseFloat(element))
  }

  render() {
    return (
      <div>
        {this.state.verified}
        <Helmet
          title={config.siteTitle}
          meta={[
          { name: 'description', content: this.i18n.strings.meta.description },
          { name: 'keywords', content: 'Recycle, Earn Money, Save Environment, Waste Items' },
          ]}
        />
        <Navbar routes={this.props.route} language={this.props.language} />
        <section id="list_items_seller" className="container">
          <div className="row">

            {/* Page Heading/Breadcrumbs */}
            <div className="row">
              <div className="col-lg-12">
                <h1 className="page-header">{this.i18n.strings.buyer_dashboard.title} - {this.state.type}</h1>
              </div>
            </div>
            {/*  /.row */}

            {/*  List Of Items */}
            {
              Object.keys(this.state.cityTotals)
              .map(
              (cityName) => {
                const city = this.state.cityTotals[cityName]
                return (
                  <WasteItem i18n={this.i18n} name={cityName} quantity={city * 0.7} price={this.state.price} />
                ) }
              )
            }
          </div>
          {/*  List Of Sellers   */}
          <article className="wrapper">
            <header className="page-header">
              <h3>{this.i18n.strings.buyer_dashboard.table_title}</h3>
            </header>

            <main id="table" className="table-editable">

              <div className="panel panel-default">
                <div className="panel_list_seller_buyer panel-heading">List of Upcoming Transactions</div>
                <Table bordered hover striped responsive>
                  <thead>
                    <tr className="table_head">
                      <th>Date</th>
                      <th>Time</th>
                      <th>Quantity</th>
                      <th>Total Cost</th>
                      <th>Location</th>
                    </tr>
                  </thead>
                  <tbody className="table_body">
                    {/*{Object.keys(this.state.users).map((key, index) => {
                      const user = this.state.users[key]
                      let quantity = 0
                       if(user.items){
                         if(user.items['Used Cooking Oil']){
                           quantity = user.items['Used Cooking Oil'].quantity
                         }
                       }
                      return (<SellerEntry key={index} name={user.fullname} quantity={quantity} />)
                    })}*/}
                    <tr>
                      <td>11/10/2016</td>
                      <td>6 PM</td>
                      <td>1000 KG</td>
                      <td>RM 1000</td>
                      <td>Shah Alam</td>
                    </tr>
                    <tr>
                      <td>16/10/2016</td>
                      <td>8 PM</td>
                      <td>3000 KG</td>
                      <td>RM 3000</td>
                      <td>Subang Jaya</td>
                    </tr>
                    <tr>
                      <td>21/10/2016</td>
                      <td>8 PM</td>
                      <td>4000 KG</td>
                      <td>RM 4000</td>
                      <td>Puchong</td>
                    </tr>
                    <tr>
                      <td>1/11/2016</td>
                      <td>7 PM</td>
                      <td>2000 KG</td>
                      <td>RM 2000</td>
                      <td>Kelana Jaya</td>
                    </tr>
                  </tbody>
                </Table>
              </div>
            </main>
          </article>

          {/*}<div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">{this.i18n.strings.buyer_dashboard.total_costs}</h3>
            </div>
            <div className="panel-total-pricing panel-body">
                 RM 160
            </div>
          </div>
          */}
          <DashboardReference i18n={this.i18n} />
        </section>
        <Footer language={this.props.language} />
      </div>
    )
  }
}

BuyerDashboard.propTypes = {
  language: React.PropTypes.string,
}

export default BuyerDashboard
