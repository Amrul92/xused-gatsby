import React from 'react'
import { Link } from 'react-router'
import { Navbar, Grid, Row, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import { prefixLink } from 'gatsby-helpers'

import firebase from '../utils/firebase'
import logo from '../images/logos/XUsed_Header.png'
import I18n from '../utils/i18n'

class XUsedNav extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      user: '',
      fullname: '',
      buyer: false,
    }
  }

  componentDidMount() {
    this.authListener = firebase.auth().onAuthStateChanged(user =>
      user ? firebase.database().ref(`/buyers/${user.uid}/`).on('value', (r) => {
        this.setState({ user, buyer: r.val() ? true : false })
      }) : ''
    )
  }

  componentWillUnmount() {
    //this.setState(null)
    console.log('unmounting')
    //this.authListener();
  }

  privateRoutes(i18n) {
    let dropdown
    if (this.state.user) {
      dropdown = (
        <NavDropdown
          eventKey={3}
          title={this.state.user.displayName}
          id="basic-nav-dropdown"
        >
          <LinkContainer to={i18n.link(`${this.state.buyer ? 'buyer_' : ''}profile`)}>
            <MenuItem eventKey={3.1}>{i18n.strings.navbar.profile}</MenuItem>
          </LinkContainer>
          { /* Works if not index, FIXME: wrap properly */ }
          { /* <LinkContainer to={i18n.link('')}> */ }
          <MenuItem
            eventKey={3.3}
            onClick={() => {firebase.auth().signOut(); window.location.pathname = ''}}
          >
            {i18n.strings.navbar.sign_out}
          </MenuItem>
          { /* </LinkContainer> */}
        </NavDropdown>)
    } else {
      dropdown = (<div />)
    }
    return dropdown
  }

  dashboardOrLogin(i18n) {
    let result;
    if (!this.state.user) {
      result = (
        <Nav pullRight>
          <LinkContainer to={i18n.link('register')}>
            <NavItem
              eventKey={1}
              id="basic-nav-dropdown"
            >
              {i18n.strings.navbar.sign_up}
            </NavItem>
          </LinkContainer>
          <LinkContainer to={i18n.link('login')}>
            <NavItem
              eventKey={1}
              id="basic-nav-dropdown"
            >
              {i18n.strings.navbar.sign_in}
            </NavItem>
          </LinkContainer>
        </Nav>
      )
    } else if (this.state.buyer) {
      result = ''
      /* To re-enable buyer dashboard link
      (
        <LinkContainer to={i18n.link('buyer_dashboard')}>
          <NavItem
            eventKey={1}
            id="basic-nav-dropdown"
          >
            {i18n.strings.navbar.my_dashboard}
          </NavItem>
        </LinkContainer>
      )
      */
    } else {
      result = (
        <LinkContainer to={i18n.link('seller_dashboard')}>
          <NavItem
            eventKey={1}
            id="basic-nav-dropdown"
          >
            {i18n.strings.navbar.my_dashboard}
          </NavItem>
        </LinkContainer>
      )
    }

    return result
  }

  languageSelect(lang) {
    const langMap = {
      en: 'English',
      ms: 'Malay',
    }

    const rootOrSubdir = () => {
      if (!this.props.routes){ return '' }
      let subdir = this.props.routes.path || this.props.routes.pathname
      subdir = subdir.split('/')[2]
      return subdir ? subdir + '/' : ''
    }

    const MenuItems = Object.keys(langMap).filter(k => k !== lang)
      .map((k, index) => (
        <LinkContainer key={k + index} to={`/${k}/${rootOrSubdir()}`}>
          <MenuItem eventKey={3 + (0.1 * index)}> {langMap[k]} </MenuItem>
        </LinkContainer>
      ))

    return (
      <NavDropdown eventKey={3} title={langMap[lang]} id="basic-nav-dropdown">
        { MenuItems }
      </NavDropdown>
    )
  }

  render() {
    const i18n = new I18n(this.props.language)
    return (
      <Navbar id="navbar_custom" inverse staticTop>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="/en/">
              <img src={logo} alt="XUsed Logo" />
            </a>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            {this.languageSelect(this.props.language)}
          </Nav>
          <Nav pullRight>
            {this.dashboardOrLogin(i18n)}
            {this.privateRoutes(i18n)}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}

export default XUsedNav
