import React from 'react'
import { Button, Well } from 'react-bootstrap'

class OtherCountry extends React.Component {
  consructor(props) {
    super(props)
    this.state = {
      email: '',
      country: '',
    }
  }
  render() {
    return (
      <div className="interested_user">
        <div>
          <Well bsSize="large">
            <p className="notify_header">{this.i18n.strings.register.notify_header}</p>
            <div className="form-group">
              <label htmlFor="email"> {this.i18n.strings.register.email} </label>
              <input
                id="email"
                className="form_notify form-control"
                required
                value={this.state.email}
                onChange={this.handleChange('email')}
                placeholder={this.i18n.strings.register.email}
              />
            </div>
            <div className="form-group">
              <label htmlFor="address">{this.i18n.strings.profile.country}</label>
              <input
                id="address"
                className="form_notify form-control"
                type="textarea"
                label="textarea"
                value={this.state.country}
                onChange={this.handleChange('country')}
                placeholder={this.i18n.strings.profile.country_p}
              />
            </div>
            <p>
              <Button bsStyle="success">{this.i18n.strings.register.notify_submit}</Button>
            </p>
          </Well>
        </div>
      </div>
    )
  }
}


export default OtherCountry
