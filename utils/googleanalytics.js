import reactGA from 'react-ga'

reactGA.initialize('UA-83944239-1')

const trackPage = (uid) => {
    reactGA.set({ page: window.location.href, uid })
    reactGA.pageview(window.location.pathname)
}

const trackModal = (modal, uid) => {
    reactGA.set({ page: window.location.href, uid })
    reactGA.modalview(modal)
}

const event = (uid, e) => {
    reactGA.set({ page: window.location.href, uid })
    reactGA.event(e)
}

export default { trackPage, trackModal, event }
