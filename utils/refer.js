const getQuery = () => {
  if (typeof window === 'undefined'){
    global.window = null
  } else if (window && window.location && window.location.pathname) {
      const result = window.location.href.split('?')[1] || ''
    return result
  }
  return ''
}

const hasRefer = () => {
    let result = getQuery()
        .split('&')
        .map( q => {
            const res = {}
            const pair = q.split('=')
            res[pair[0]] = pair[1]
            return res
        }
            )
    result = result.filter(args => args)
    result = result.filter(args => args.ref)[0]
    return result
}

export default hasRefer
