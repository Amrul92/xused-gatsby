// From https://github.com/jakubrohleder/loko-landing/blob/master/utils/i18n.js

import { config } from 'config'

const i18n = {}
config.langs.forEach((lang) => {
  i18n[lang] = require(`../i18n/${lang}`)
})

i18n.link = (lang, path) => `/${lang}/${path ? `${path}/` : ''}`

export default language => ({
  language: language || config.lang,
  strings: i18n[language || config.lang],
  link: path => i18n.link(language || config.lang, path),
})

export const all = i18n
