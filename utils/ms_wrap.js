import React from 'react'

export default Comp => {
  return props => (
    <Comp {...props} language="ms" />
  )
}
