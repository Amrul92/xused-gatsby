import * as Firebase from 'firebase';

// Initialize Firebase
const config = {
  apiKey: 'AIzaSyAh0el2YksK8nNlXV90VgxkxiZYfDshijg',
  authDomain: 'xused-3852c.firebaseapp.com',
  databaseURL: 'https://xused-3852c.firebaseio.com',
  storageBucket: 'gs://xused-3852c.appspot.com/',
};

Firebase.initializeApp(config)

export default Firebase
