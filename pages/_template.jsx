import React from 'react'

// TODO: See if this is better than Bootstrap's grid?
// import { Container } from 'react-responsive-grid'
import { Link } from 'react-router'
import { prefixLink } from 'gatsby-helpers'
import { Navbar, Grid, Row, Nav, NavItem } from 'react-bootstrap'
import '../css/markdown-styles.css'
import logo from '../images/logos/XUsed_Header.png'
import firebase from '../utils/firebase'

import { rhythm } from '../utils/typography'

let reactGA = false
let uid = false

if (typeof window !== 'undefined') {
  reactGA = require('../utils/googleanalytics')
  uid = firebase.auth().currentUser ? firebase.auth().currentUser.uid : false
}

const container = props => {
  if (reactGA) { reactGA.trackPage(uid) }

  return (<div>
    {/* Content */}
    {props.children}
  </div>)
}

container.propTypes = {
  children: React.PropTypes.any,
}

export default container;
