import marketplace from './marketplace.jpg'
import features1_en from './features1_en.png'
import features2_en from './features2_en.png'
import cans from './cans.png'
import paper from './paper.png'
import uco from './uco.png'
import glass from './glass.png'
import food from './food.jpg'
import plastic from './plastic.png'

export default { marketplace,
  features1_en,
  features2_en,
  cans,
  paper,
  uco,
  glass,
  food,
  plastic,
}
