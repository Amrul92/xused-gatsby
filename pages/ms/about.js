import about from '../../templates/about.js'
import msWrap from './../../utils/ms_wrap'

export default msWrap(about)
