import buyerProfile from '../../templates/buyer_profile.js'
import msWrap from '../../utils/ms_wrap'

export default msWrap(buyerProfile)
