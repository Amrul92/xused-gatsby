import Faq from '../../templates/faq.js'
import msWrap from './../../utils/ms_wrap.js'

export default msWrap(Faq)
