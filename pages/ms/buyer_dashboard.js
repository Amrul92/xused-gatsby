import buyer from '../../templates/buyer_dashboard.js'
import msWrap from './../../utils/ms_wrap'

export default msWrap(buyer)
