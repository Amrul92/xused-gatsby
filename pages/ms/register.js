import Register from '../../templates/register.js'
import msWrap from '../../utils/ms_wrap'

export default msWrap(Register)
