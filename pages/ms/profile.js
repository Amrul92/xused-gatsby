import profile from '../../templates/profile.js'
import msWrap from './../../utils/ms_wrap'

export default msWrap(profile)
