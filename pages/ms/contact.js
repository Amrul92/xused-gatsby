import React from 'react'
import contact from '../../templates/contact.js'
import msWrap from './../../utils/ms_wrap.js'

const ms_contact = msWrap(contact)

export default ms_contact
