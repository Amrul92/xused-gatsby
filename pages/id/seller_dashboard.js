import seller from '../../templates/seller_dashboard'
import idWrap from '../../utils/id_wrap'

export default idWrap(seller)
