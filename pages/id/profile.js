import profile from '../../templates/profile.js'
import idWrap from '../../utils/id_wrap'

export default idWrap(profile)
