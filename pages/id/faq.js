import Faq from '../../templates/faq.js'
import idWrap from '../../utils/id_wrap.js'

export default idWrap(Faq)
