import buyerProfile from '../../templates/buyer_profile.js'
import idWrap from '../../utils/id_wrap'

export default idWrap(buyerProfile)
