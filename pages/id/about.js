import about from '../../templates/about.js'
import idWrap from '../../utils/id_wrap'

export default idWrap(about)
