import Register from '../../templates/register.js'
import idWrap from '../../utils/id_wrap'

export default idWrap(Register)
