import buyer from '../../templates/buyer_dashboard.js'
import idWrap from '../../utils/id_wrap'

export default idWrap(buyer)
