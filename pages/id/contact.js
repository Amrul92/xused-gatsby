import React from 'react'
import contact from '../../templates/contact.js'
import idWrap from '../../utils/id_wrap.js'

const id_contact = idWrap(contact)

export default id_contact
