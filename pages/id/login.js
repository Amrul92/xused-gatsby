import Login from '../../templates/login.js'
import idWrap from '../../utils/id_wrap'

export default idWrap(Login)
