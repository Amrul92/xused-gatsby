import index from '../../templates/index.js'
import idWrap from '../../utils/id_wrap.js'

export default idWrap(index)
