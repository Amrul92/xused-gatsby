import index from '../../templates/index.js'
import enWrap from '../../utils/en_wrap.js'

export default enWrap(index)
