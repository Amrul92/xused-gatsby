import buyer from '../../templates/buyer_dashboard.js'
import enWrap from '../../utils/en_wrap'

export default enWrap(buyer)
