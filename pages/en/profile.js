import profile from '../../templates/profile.js'
import enWrap from '../../utils/en_wrap'

export default enWrap(profile)
