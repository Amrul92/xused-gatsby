import Register from '../../templates/register.js'
import enWrap from '../../utils/en_wrap'

export default enWrap(Register)
