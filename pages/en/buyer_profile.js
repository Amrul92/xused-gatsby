import buyerProfile from '../../templates/buyer_profile.js'
import enWrap from '../../utils/en_wrap'

export default enWrap(buyerProfile)
