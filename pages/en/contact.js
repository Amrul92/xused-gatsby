import React from 'react'
import contact from '../../templates/contact.js'
import enWrap from '../../utils/en_wrap.js'

const en_contact = enWrap(contact)

export default en_contact
