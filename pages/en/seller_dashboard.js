import seller from '../../templates/seller_dashboard'
import enWrap from '../../utils/en_wrap'

export default enWrap(seller)
