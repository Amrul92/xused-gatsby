import about from '../../templates/about.js'
import enWrap from '../../utils/en_wrap'

export default enWrap(about)
