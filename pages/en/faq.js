import React from 'react'
import Faq from '../../templates/faq.js'
import enWrap from '../../utils/en_wrap.js'

export default enWrap(Faq)
