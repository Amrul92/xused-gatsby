import Login from '../../templates/login.js'
import enWrap from '../../utils/en_wrap'

export default enWrap(Login)
